export function distanceOfPointFromSegment(
  px: number,
  py: number,
  vx: number,
  vy: number,
  wx: number,
  wy: number,
) {
  const l2 = distanceBetweenPoints(vx, vy, wx, wy);
  if (l2 === 0) return distanceBetweenPoints(px, py, vx, vy);
  let t = ((px - vx) * (wx - vx) + (py - vy) * (wy - vy)) / l2;
  t = Math.max(0, Math.min(1, t));
  return distanceBetweenPoints(px, py, vx + t * (wx - vx), vy + t * (wy - vy));
}

export function distanceBetweenPoints(
  vx: number,
  vy: number,
  wx: number,
  wy: number,
) {
  return Math.pow(vx - wx, 2) + Math.pow(vy - wy, 2);
}
