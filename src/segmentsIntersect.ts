export function segmentsIntersect(
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  x3: number,
  y3: number,
  x4: number,
  y4: number,
) {
  // Get the orientation of points p3 and p4 in relation
  // to the line segment (p1, p2)
  const o1 = orientation(x1, y1, x2, y2, x3, y3);
  const o2 = orientation(x1, y1, x2, y2, x4, y4);
  const o3 = orientation(x3, y3, x4, y4, x1, y1);
  const o4 = orientation(x3, y3, x4, y4, x2, y2);

  // If the points p1, p2 are on opposite sides of the infinite
  // line formed by (p3, p4) and conversly p3, p4 are on opposite
  // sides of the infinite line formed by (p1, p2) then there is
  // an intersection.
  if (o1 !== o2 && o3 !== o4) {
    return true;
  }

  // Collinear special cases
  if (o1 === 0 && pointOnLine(x1, y1, x2, y2, x3, y3)) {
    return true;
  }
  if (o2 === 0 && pointOnLine(x1, y1, x2, y2, x4, y4)) {
    return true;
  }
  if (o3 === 0 && pointOnLine(x3, y3, x4, y4, x1, y1)) {
    return true;
  }
  if (o4 === 0 && pointOnLine(x3, y3, x4, y4, x2, y2)) {
    return true;
  }

  return false;
}

function orientation(
  x0: number,
  y0: number,
  x1: number,
  y1: number,
  x2: number,
  y2: number,
) {
  const value = (y1 - y0) * (x2 - x1) - (x1 - x0) * (y2 - y1);
  if (Math.abs(value) < Number.EPSILON) {
    return 0;
  }
  return value > 0 ? -1 : +1;
}

function pointOnLine(
  x0: number,
  y0: number,
  x1: number,
  y1: number,
  x2: number,
  y2: number,
) {
  return (
    orientation(x0, y0, x1, y1, x2, y2) === 0 &&
    Math.min(x0, x1) <= x2 &&
    x2 <= Math.max(x0, x1) &&
    Math.min(y0, y1) <= y2 &&
    y2 <= Math.max(y0, y1)
  );
}
