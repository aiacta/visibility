export function angleBetween(
  x0: number,
  y0: number,
  x1: number,
  y1: number,
): number {
  const r = Math.atan2(y1 - y0, x1 - x0);
  return (r + Math.PI) % (Math.PI * 2);
}
