import { angleBetween } from './angleBetween';
import { distanceBetweenPoints, distanceOfPointFromSegment } from './distances';
import { lineIntersection } from './lineIntersection';
import { segmentsIntersect } from './segmentsIntersect';

export function lineOfSight(actors: Point[], segments: Segment[]): Triangle[] {
  const triangles: Triangle[] = [];

  actors.forEach(origin => {
    // create a list of all points, sorted by their angle to the origin
    const points = segments
      .flatMap(segment => {
        let { start, end } = segment;
        let startAngle = angleBetween(start.x, start.y, origin.x, origin.y);
        let endAngle = angleBetween(end.x, end.y, origin.x, origin.y);
        if (startAngle > endAngle) {
          [start, end] = [end, start];
          [startAngle, endAngle] = [endAngle, startAngle];
        }
        // 0 — 2*PI
        let dAngle = endAngle - startAngle;
        if (dAngle > Math.PI) {
          [start, end] = [end, start];
          [startAngle, endAngle] = [endAngle, startAngle];
        }
        return [
          {
            position: start,
            isStart: true,
            angle: startAngle,
            segment,
          },
          {
            position: end,
            isStart: false,
            angle: endAngle,
            segment,
          },
        ];
      })
      .sort((a, b) => {
        const d = a.angle - b.angle;
        if (Math.abs(d) < Number.EPSILON) {
          if (a.isStart && b.isStart) {
            const bDist = distanceOfPointFromSegment(
              origin.x,
              origin.y,
              b.segment.start.x,
              b.segment.start.y,
              b.segment.end.x,
              b.segment.end.y,
            );
            const aDist = distanceOfPointFromSegment(
              origin.x,
              origin.y,
              a.segment.start.x,
              a.segment.start.y,
              a.segment.end.x,
              a.segment.end.y,
            );
            return bDist - aDist;
          }
          return a.isStart ? -1 : 1;
        }
        return d;
      });

    const currentSegments = segments
      .filter(
        segment =>
          segmentsIntersect(
            origin.x,
            origin.y,
            origin.x + Number.MAX_SAFE_INTEGER,
            origin.y,
            segment.start.x,
            segment.start.y,
            segment.end.x,
            segment.end.y,
          ) && Math.abs(segment.end.y - origin.y) > Number.EPSILON,
      )
      .sort((a, b) => {
        const [px0, py0] = lineIntersection(
          origin.x,
          origin.y,
          origin.x + Number.MAX_SAFE_INTEGER,
          origin.y,
          a.start.x,
          a.start.y,
          a.end.x,
          a.end.y,
        );
        const [px1, py1] = lineIntersection(
          origin.x,
          origin.y,
          origin.x + Number.MAX_SAFE_INTEGER,
          origin.y,
          b.start.x,
          b.start.y,
          b.end.x,
          b.end.y,
        );
        return (
          distanceBetweenPoints(origin.x, origin.y, px0, py0) -
          distanceBetweenPoints(origin.x, origin.y, px1, py1)
        );
      });

    let angle = 0;

    points.forEach(point => {
      const nearestSegment = currentSegments[0];
      if (point.isStart) {
        if (!currentSegments.includes(point.segment)) {
          // find the first segment that is behind the segment of the current point
          const idx = currentSegments.findIndex(seg => {
            const [px, py] = lineIntersection(
              origin.x,
              origin.y,
              point.position.x,
              point.position.y,
              seg.start.x,
              seg.start.y,
              seg.end.x,
              seg.end.y,
            );
            return (
              distanceBetweenPoints(origin.x, origin.y, px, py) >=
              distanceBetweenPoints(
                origin.x,
                origin.y,
                point.position.x,
                point.position.y,
              )
            );
          });
          switch (idx) {
            case -1:
              // all other segments are infront, so we append this segment as last
              currentSegments.push(point.segment);
              break;
            default:
              // insert segment in correct order
              currentSegments.splice(idx, 0, point.segment);
              break;
          }
        }
      } else {
        // remove finished segment from current list
        const idx = currentSegments.indexOf(point.segment);
        if (idx >= 0) {
          currentSegments.splice(idx, 1);
        }
      }

      // if the active segment changed, we need to draw a triangle
      if (nearestSegment !== currentSegments[0]) {
        const [x1, y1] = lineIntersection(
          nearestSegment.start.x,
          nearestSegment.start.y,
          nearestSegment.end.x,
          nearestSegment.end.y,
          origin.x,
          origin.y,
          origin.x + Math.cos(angle),
          origin.y + Math.sin(angle),
        );

        const [x2, y2] = lineIntersection(
          nearestSegment.start.x,
          nearestSegment.start.y,
          nearestSegment.end.x,
          nearestSegment.end.y,
          origin.x,
          origin.y,
          origin.x + Math.cos(point.angle),
          origin.y + Math.sin(point.angle),
        );

        triangles.push([origin.x, origin.y, x1, y1, x2, y2]);

        angle = point.angle;
      }
    });

    // if we have an open segment left, draw triangle
    if (currentSegments.length > 0) {
      const [x1, y1] = lineIntersection(
        currentSegments[0].start.x,
        currentSegments[0].start.y,
        currentSegments[0].end.x,
        currentSegments[0].end.y,
        origin.x,
        origin.y,
        origin.x + Math.cos(angle),
        origin.y + Math.sin(angle),
      );
      const [x2, y2] = lineIntersection(
        currentSegments[0].start.x,
        currentSegments[0].start.y,
        currentSegments[0].end.x,
        currentSegments[0].end.y,
        origin.x,
        origin.y,
        origin.x + 1,
        origin.y,
      );

      triangles.push([origin.x, origin.y, x1, y1, x2, y2]);
    }
  });

  return triangles;
}

interface Point {
  x: number;
  y: number;
}

interface Segment {
  start: Point;
  end: Point;
}

type Triangle = [number, number, number, number, number, number];
