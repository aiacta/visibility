import { lineOfSight } from '../src/visibility';

describe('lineOfSight', () => {
  it('should calculate the correct line of sight', () => {
    expect(lineOfSight(actors, segments)).toMatchSnapshot();
  });
});

const actors = [{ x: 550, y: 300 }, { x: 100, y: 200 }, { x: 300, y: 250 }];

const segments = [
  { start: { x: 50, y: 50 }, end: { x: 750, y: 50 } },
  { start: { x: 750, y: 50 }, end: { x: 750, y: 550 } },
  { start: { x: 750, y: 550 }, end: { x: 50, y: 550 } },
  { start: { x: 50, y: 550 }, end: { x: 50, y: 50 } },
  { start: { x: 200, y: 200 }, end: { x: 300, y: 200 } },
  { start: { x: 300, y: 200 }, end: { x: 350, y: 250 } },
  { start: { x: 350, y: 250 }, end: { x: 350, y: 280 } },
  { start: { x: 350, y: 280 }, end: { x: 300, y: 280 } },
  { start: { x: 600, y: 300 }, end: { x: 700, y: 300 } },
  { start: { x: 700, y: 300 }, end: { x: 700, y: 400 } },
  { start: { x: 700, y: 400 }, end: { x: 600, y: 400 } },
  { start: { x: 600, y: 400 }, end: { x: 600, y: 300 } },
  { start: { x: 205, y: 264 }, end: { x: 120, y: 335 } },
  { start: { x: 120, y: 335 }, end: { x: 121, y: 456 } },
  { start: { x: 121, y: 456 }, end: { x: 387, y: 460 } },
  { start: { x: 387, y: 460 }, end: { x: 387, y: 328 } },
  { start: { x: 578, y: 115 }, end: { x: 448, y: 225 } },
  { start: { x: 448, y: 225 }, end: { x: 490, y: 371 } },
  { start: { x: 490, y: 371 }, end: { x: 537, y: 384 } },
  { start: { x: 187, y: 81 }, end: { x: 353, y: 87 } },
  { start: { x: 353, y: 87 }, end: { x: 370, y: 157 } },
  { start: { x: 370, y: 157 }, end: { x: 474, y: 108 } },
  { start: { x: 297, y: 134 }, end: { x: 155, y: 134 } },
  { start: { x: 155, y: 134 }, end: { x: 116, y: 254 } },
  { start: { x: 116, y: 254 }, end: { x: 95, y: 318 } },
  { start: { x: 95, y: 318 }, end: { x: 77, y: 464 } },
  { start: { x: 77, y: 464 }, end: { x: 137, y: 510 } },
  { start: { x: 137, y: 510 }, end: { x: 472, y: 493 } },
  { start: { x: 472, y: 493 }, end: { x: 441, y: 412 } },
  { start: { x: 441, y: 412 }, end: { x: 530, y: 422 } },
  { start: { x: 530, y: 422 }, end: { x: 588, y: 530 } },
  { start: { x: 588, y: 530 }, end: { x: 701, y: 529 } },
  { start: { x: 701, y: 529 }, end: { x: 724, y: 374 } },
  { start: { x: 724, y: 374 }, end: { x: 729, y: 238 } },
  { start: { x: 729, y: 238 }, end: { x: 571, y: 248 } },
  { start: { x: 571, y: 248 }, end: { x: 567, y: 197 } },
];
