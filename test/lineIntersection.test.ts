import { lineIntersection } from '../src/lineIntersection';

describe('lineIntersection', () => {
  it('should calculate the correct intersections', () => {
    expect(lineIntersection(0, 0, 10, 0, 5, 10, 5, -10)).toEqual([5, 0]);
  });
});
