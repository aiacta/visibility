import { segmentsIntersect } from '../src/segmentsIntersect';

describe('segmentsIntersect', () => {
  it('should calculate the correct intersections', () => {
    expect(segmentsIntersect(0, 0, 10, 0, 5, 10, 5, -10)).toBe(true);
    expect(segmentsIntersect(0, 0, 10, 0, 5, 10, 5, 1)).toBe(false);
  });

  it('should be true for collinear special cases', () => {
    expect(segmentsIntersect(0, 0, 10, 0, 0, 0, 0, -10)).toBe(true);
    expect(segmentsIntersect(0, 0, 10, 0, 0, 0, 0, 0)).toBe(true);
  });
});
