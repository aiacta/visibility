import { angleBetween } from '../src/angleBetween';

describe('angleBetween', () => {
  it('should calculate an angle', () => {
    expect(angleBetween(0, 0, 1, 0)).toBe(Math.PI);
    expect(angleBetween(0, 0, -1, 0)).toBe(0);
    expect(angleBetween(0, 0, -1, -1)).toBe(Math.PI / 4);
    expect(angleBetween(0, 0, 0, -1)).toBe(Math.PI / 2);
  });
});
