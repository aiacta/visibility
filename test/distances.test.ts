import {
  distanceBetweenPoints,
  distanceOfPointFromSegment,
} from '../src/distances';

describe('distanceBetweenPoints', () => {
  it('should calculate the correct distances between points', () => {
    expect(distanceBetweenPoints(0, 0, 0, 1)).toBe(1);
    expect(distanceBetweenPoints(0, 0, -1, 0)).toBe(1);
    expect(distanceBetweenPoints(0, 0, 0, 10)).toBe(10 ** 2);
    expect(distanceBetweenPoints(0, 0, 5, 5)).toBe(50);
    expect(distanceBetweenPoints(0, 0, -250, 10)).toBe(62600);
  });
});

describe('distanceOfPointFromSegment', () => {
  it('should calculate the correct distances between segment', () => {
    expect(distanceOfPointFromSegment(0, 0, 5, -5, 5, 5)).toBe(25);
    expect(distanceOfPointFromSegment(0, 0, 0, 0, 5, 5)).toBe(0);
    expect(distanceOfPointFromSegment(0, 0, 5, 5, 5, 5)).toBe(50);
    expect(distanceOfPointFromSegment(0, 0, 0, -5, 5, 5)).toBe(5);
  });
});
